Feature: Gists

  @Excute1
  Scenario Outline: As a user, I want to create a public gists
    Given User Git Login
    When User do create Gists and input data
    Then I should see my new gists
   
    Examples: 
      | yourDesc                                 | fileName            | codeHere                              |
      | Test Create                              | Test.java           | System.out.println("Test");           |
    	  
  @Excute2
  Scenario Outline: As a user, I want to edit an existing gists
    Given User Git Login
    When User do edit Gists and input data
    Then I should see my edited gists
   
    Examples: 
      | editComment                               | 
      | Test edit                                 | 
     
  @Excute3
  Scenario Outline: As a user, I want to delete an existing gists
    Given User Git Login
    When User do delete Gists 
    Then My git successfully deleted
   
 
   	  
  @Excute4
  Scenario Outline: As a user, I want to see my list of gists
    Given User Git Login
    When User do check list Gists
    Then i can see list
   
   