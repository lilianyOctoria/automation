package runner.parallel;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(strict = true, 
glue = { "stepdefinitions" }, 
features = { "@target/rerun_featureName.txt" }, 
plugin = { "json:target/cucumber/json/cucumber-report-composite.json", "pretty",
        "html:target/cucumber/"})

public class ReRun {

	
	
}