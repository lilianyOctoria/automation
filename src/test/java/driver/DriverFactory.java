package driver;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
//import org.openqa.selenium.remote.FileDetector;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import cucumber.api.Scenario;
import page.Settings;

public final class DriverFactory {
	private static RemoteWebDriver driver;
	private static String defaultWindowHandle;
	static Scenario scenario;

	public final static WebDriver getWebDriver() {
		if (driver == null) {
			try {
				createNewDriverInstance();
				//OpenBrowser();
			} catch (Exception e) {
				System.out.println("Error on creating driver");
				e.printStackTrace();
			}

		}
		return driver;
	}

	public final static WebDriver getWebDriverMozilla() {
		if (driver == null) {
			try {
				OpenBrowser();
			} catch (Exception e) {
				System.out.println("Error on creating driver");
				e.printStackTrace();
			}

		}
		return driver;
	}

	public static void createNewDriverInstance() throws MalformedURLException {
		System.setProperty("webdriver.gecko.driver",
				new File("drivers/geckodriver-v0.19.0-win64.exe").getAbsolutePath());
		final DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setJavascriptEnabled(true);
		capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		capabilities.setCapability("acceptInsecureCerts", true);
		capabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);
		ChromeOptions chromeOptions = new ChromeOptions();
		chromeOptions.addArguments("start-maximized");
		// capabilities.setCapability(ChromeOptions.CAPABILITY , chromeOptions);

		final URL remoteAddress = new URL(Settings.getDriverRemoteURL());
		driver = new RemoteWebDriver(remoteAddress, capabilities);
		driver.setFileDetector(new LocalFileDetector());
		//driver = new ChromeDriver(capabilities);
		driver.manage().timeouts().implicitlyWait(page.Settings.getDefaultTimeOut(), TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(180, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		defaultWindowHandle = driver.getWindowHandle();
	}

	public static void OpenBrowser() throws IOException {
		System.setProperty("webdriver.gecko.driver", new File("drivers/geckodriver-1.7-64.exe").getAbsolutePath());
		final DesiredCapabilities capabilities = DesiredCapabilities.firefox();
		WebDriver driver = new FirefoxDriver();
		driver.navigate().to(Settings.getHomePageURL());
		capabilities.setJavascriptEnabled(true);
		capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		capabilities.setCapability("acceptInsecureCerts", true);

		/*
		 * driver.manage().timeouts().implicitlyWait(100, TimeUnit.SECONDS);
		 * driver.manage().timeouts().pageLoadTimeout(300, TimeUnit.SECONDS);
		 */
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		defaultWindowHandle = driver.getWindowHandle();

	}

	public static String getDefaultWindowHandle() {
		return defaultWindowHandle;
	}

	public static void tearDown() {
		if (driver != null) {
			try {
				driver.quit();
				Thread.sleep(5000);
			} catch (Exception e) {
				//
			} finally {
				driver = null;
			}
		}
	}

}
