package page;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import cucumber.api.Scenario;



public class Settings {
	
	final private static long defaultTimeOut = 50;
	static String homePageURL;
	static String userNameGit ;
	static String PasswordGit;
	
	private static Scenario currentScenario;
	private static String currentScreencaptureFolderPath;

	public static String getHomePageURL() throws IOException {
		Properties prop = new Properties();
		InputStream input = new FileInputStream("Config.properties");
		prop.load(input);
		return homePageURL = prop.getProperty("homePageURL");
	}


        public static String getusernameGit()throws IOException{
		Properties prop = new Properties();
		InputStream input = new FileInputStream("Config.properties");
		prop.load(input);
		return userNameGit = prop.getProperty("userNameGit");
	}
	
	public static String getpasswordGit()throws IOException{
		Properties prop = new Properties();
		InputStream input = new FileInputStream("Config.properties");
		prop.load(input);
		return PasswordGit = prop.getProperty("PasswordGit");
	}
	
	
	

}
