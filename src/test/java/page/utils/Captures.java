package page.utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import cucumber.api.Scenario;
import driver.DriverFactory;
import gherkin.formatter.model.ScenarioOutline;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.cropper.indent.BlurFilter;
import ru.yandex.qatools.ashot.cropper.indent.IndentCropper;

import ru.yandex.qatools.ashot.shooting.ShootingStrategies;

import cucumber.api.Scenario;

public final class Captures {

	String name;

	//private static final javax.imageio.stream.ImageOutputStream ImageOutputStream = null;

	
	
	public static void takeSimpleFullScreenShot() throws IOException {
		//Captures.takeScreenShot(DriverFactory.getWebDriver(), Captures.getSimpleCapturePath());
	
		Screenshot bi = new AShot().shootingStrategy(ShootingStrategies.viewportPasting(50)).takeScreenshot(DriverFactory.getWebDriver());
		createFolderIfDoesNotExist((new File(Captures.getSimpleCapturePath())).getParent());
		File compressedImageFile = new File(Captures.getSimpleCapturePath());
		OutputStream outputStream = new FileOutputStream(compressedImageFile);
	    ImageIO.write(bi.getImage(), "png", outputStream);
		
		
	}


	
	public static void compressAndSaveImage(BufferedImage image, String filePath) {
		try {

			File compressedImageFile = new File(filePath);
			OutputStream outputStream = new FileOutputStream(compressedImageFile);

			Iterator<ImageWriter> writers = ImageIO
					.getImageWritersByFormatName(FilenameUtils.getExtension(filePath).toLowerCase());
			ImageWriter imageWriter = (ImageWriter) writers.next();

			ImageOutputStream imageOutputStream = ImageIO.createImageOutputStream(outputStream);
			imageWriter.setOutput(imageOutputStream);
			ImageWriteParam imageWriteParam = imageWriter.getDefaultWriteParam();
			imageWriteParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
			imageWriteParam.setCompressionQuality(0.8f);

			int iw = image.getWidth();
			int ih = image.getHeight();
			BufferedImage newimage = new BufferedImage(iw, ih, BufferedImage.TYPE_INT_RGB);
			for (int x = 0; x < iw; x++) {
				for (int y = 0; y < ih; y++) {
					newimage.setRGB(x, y, image.getRGB(x, y));
				}
			}

			imageWriter.write(null, new IIOImage(newimage, null, null), imageWriteParam);

			outputStream.close();
			imageOutputStream.close();
			imageWriter.dispose();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void SaveImageToFile(BufferedImage bufferedImage, String filePath) throws Exception {
		File outputfile = new File(filePath);
		ImageIO.write(bufferedImage, FilenameUtils.getExtension(filePath).toLowerCase(), outputfile);
	}

	public static BufferedImage decodeToImage(byte[] imageBytes) {
		BufferedImage image = null;
		try {
			ByteArrayInputStream bis = new ByteArrayInputStream(imageBytes);
			image = ImageIO.read(bis);
			bis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return image;
	}
	
	public void before(Scenario scenario) {
		 name = scenario.getName();
		System.out.println("------------------------------");
		System.out.println(name);
		System.out.println("------------------------------");
	}
	public static void createFolderIfDoesNotExist(String FolderPath) {
		File theDir = new File(FolderPath);
		if (!theDir.exists()) {
			boolean result = false;
			try {
				theDir.mkdirs();
				result = true;
			} catch (SecurityException se) {
				// handle it
			}
			if (result) {
				System.out.println("DIR created");
			}
		}
	}
	
	
	public static void embedFolderImagesToScenario(String FolderPath, Scenario scenario) {
		File folder = new File(FolderPath);
		File[] files = folder.listFiles();
		Arrays.sort(files);
		for (final File fileEntry : files) {
			embedScreenShotToScenario(fileEntry, scenario);
		}
	}

	public static void embedScreenShotToScenario(File file, Scenario scenario) {
		try {
			FileInputStream in;
			in = new FileInputStream(file);
			String mimeType = (FilenameUtils.getExtension(file.getName()) == "png") ? "image/png" : "image/jpeg";
			scenario.embed(IOUtils.toByteArray(in), mimeType);
			// in.getChannel().lock().release();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String getSimpleCapturePath() {
		return getSimpleCapturePathWithoutExtention() + ".jpg";
	}

	public static String getSimpleCapturePathWithoutExtention() {
		return page.Settings.getCurrentScreenCaptureFolderPath()
				+ (new SimpleDateFormat("yyyyMMdd HH-mm-ss")).format(new Date());
	}

	public static void deleteAllScreenShots(String folderPath) throws IOException {
		FileUtils.deleteDirectory(new File(folderPath));
	}

}
